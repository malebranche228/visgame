let isOpened = false;


const menuNav = document.querySelector("#menu-nav");
const arrInp = document.querySelector("#arr-inp");
const arrRandom = document.querySelector("#arr-random")
const menuRoll = document.querySelector("#menu-roll");
const algo = document.querySelectorAll(".algo");

const accord = () => {
    if(menuNav.style.maxHeight == 0) menuNav.style.maxHeight = `${menuNav.scrollHeight}px`;
    else menuNav.style.maxHeight = null;
}

let currAlgo = null;

algo.forEach( alg => {
    alg.addEventListener("click", event => {
        const selected = event.target;
        algo.forEach( obj => {
            obj.classList.remove("selected");
        });
        selected.classList.add("selected");
        currAlgo = selected.id;
        console.log(selected.id)
        
    });
});

menuRoll.addEventListener("click", event => {
    accord();
});

arrRandom.addEventListener("click", event => {
    const arr = randomArr();
    arrInp.value = arrAsString(arr);
});

const getArray = () => {
    const res = stringToArray(arrInp.value);
    if(!res){
        alert("Provided values are not correct!");
        return null;
    }
    return res;
}
