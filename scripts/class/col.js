class Col {

    constructor(conf){
        this.val            = conf.val || defConf.val;
        this.color          = conf.color || defConf.color;
        this.valToHeight    = conf.valToHeight || defConf.valToHeight;
        this.animTick       = conf.animTick || defConf.animTick;
        this.width          = conf.width || defConf.width;
        this.padd           = conf.padd || defConf.padd;
        this.textColor      = conf.textColor || defConf.textColor;
        this.font           = conf.font || defConf.width;
        this.animPeriod     = conf.animPeriod || defConf.animPeriod;

        this.hook           = conf.hook;
        this.x              = conf.x;
        this.y              = conf.y;

        if(!this.hook){
            throw new Error("No canvas hook was defined in the 'Col' constructor!");
        }

        this.Rect = new Konva.Rect({
            x:this.x,
            y:this.y,
            width:this.width,
            height:0,
            fill:this.color,
            rotation:180
        });

        this.Text = new Konva.Text({
            text:"0",
            x:this.x - this.width,
            y:this.y + 5,
            fontSize:this.font,
            fontFamily:"Helvetica",
            fill:this.textColor
        });

        this.hook.add(this.Text);
        this.hook.add(this.Rect);
    }

    getHeight(){
        return this.val * this.valToHeight;
    }


    spawn(){
        const tween = new Konva.Tween({
            node:this.Rect,
            duration:this.animTick,
            height:this.getHeight()
        });
        tween.play();
        const anim = new Konva.Animation(frame => {
            let nv = parseInt(this.Text.text());
            if(nv == this.val) anim.stop();
            else this.Text.text((nv+1).toString());
        }, this.hook);
        anim.start();
        return true;
    }

    mod(val){
        this.val = parseInt(this.Text.text());
        const PM = val < this.val ? -1 : 1;
        this.val = val;
        const tween = new Konva.Tween({
            node:this.Rect,
            duration:this.animTick,
            height:this.getHeight()
        });
        tween.play();
        const anim = new Konva.Animation(frame => {
            let nv = parseInt(this.Text.text());
            if(nv == this.val) anim.stop();
            this.Text.text((nv + PM).toString());
        }, this.hook);
        anim.start();
    }
    
    move(x,y){

        const PM = x < this.Rect.x() ? -1 : 1;

        const anim = new Konva.Animation(frame => {
            
            let currX = this.Rect.x();
            let currXText = this.Text.x();
            if((PM == 1 && currX >= x) || (PM == -1 && currX <= x)) {
                this.Rect.x(x);
                this.Text.x(x - this.width)
                anim.stop();
            }else{
                this.Rect.x(currX + this.animPeriod * PM);
                this.Text.x(currXText + this.animPeriod * PM);
            }
            
        }, this.hook);
        
        anim.start();
    }

}

const defConf = {
    val:10,
    color:"green",
    animPeriod:15,
    valToHeight:4,
    animTick:1,
    padd:10,
    textColor:"orange",
    font:20,
    width:25
}
