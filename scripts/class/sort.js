
const bubbleSort = (viz) => {
    let inputArr = viz.vals;
    let len = inputArr.length;
    let swapped;
    let queque = [];
    do {
        swapped = false;
        for (let i = 0; i < len; i++) {
            if (inputArr[i] > inputArr[i + 1]) {
                let tmp = inputArr[i];
                inputArr[i] = inputArr[i + 1];
                inputArr[i + 1] = tmp;
                //console.log('swapped: ' + i + " " + (i+1));
                queque.push({i:i, j: i+1});
                swapped = true;
            }
        }
    } while (swapped);
    console.log(inputArr)
    return queque;
};

const cocktail = (arr) => {
    let q =[]

    let is_Sorted = true;
    while (is_Sorted){
    for (let i = 0; i< arr.length - 1; i++){
            if (arr[i] > arr[i + 1])
                {
                let temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i+1] = temp;
                q.push({i:i, j:i+1});
                is_Sorted = true;
                }
    }

    if (!is_Sorted)
        break;

    is_Sorted = false;

        for (let j = arr.length - 1; j > 0; j--){
            if (arr[j-1] > arr[j])
                {
                let temp = arr[j];
                arr[j] = arr[j - 1];
                arr[j - 1] = temp;
                q.push({i:j, j:j-1});
                is_Sorted = true;
            }
        }
    }
    return q;

}

let selectionSort = (arr) => {
    let q = [];
    let len = arr.length;
    for (let i = 0; i < len; i++) {
        let min = i;
        for (let j = i + 1; j < len; j++) {
            if (arr[min] > arr[j]) {
                min = j;
            }
        }
        if (min !== i) {
            let tmp = arr[i];
            arr[i] = arr[min];
            arr[min] = tmp;
            q.push({i:i, j:min});
        }
    }
    return q;
}

function combsort(arr)
{
    let q = [];
 function is_array_sorted(arr) {
      var sorted = true;
      for (var i = 0; i < arr.length - 1; i++) {
          if (arr[i] > arr[i + 1]) {
              sorted = false;
              break;
          }
      }
      return sorted;
  }
 
  var iteration_count = 0;
  var gap = arr.length - 2;
  var decrease_factor = 1.25;
 
  
  while (!is_array_sorted(arr)) 
  {
    
      if (iteration_count > 0)
         gap = (gap == 1) ? gap : Math.floor(gap / decrease_factor);
 
  
      var front = 0;
      var back = gap;
      while (back <= arr.length - 1) 
      {
          
        
          if (arr[front] > arr[back])
          {
              var temp = arr[front];
              arr[front] = arr[back];
              arr[back] = temp;
              q.push({i:front, j:back});
          }
 
          
        
          front += 1;
          back += 1;
      }
      iteration_count += 1;
  }
  return q;
}