class Vizualizer{

    constructor(conf){
        this.vizData = [];
        this.vals = conf.vals;
        this.stage = conf.stage;
        this.hook = conf.hook;
        this.padd = conf.padd || 30;
        this.width = conf.width || 10;
        this.y = conf.y || this.stage.height() / 2;
        this.x = conf.x || this.stage.width() / 2 - (conf.vals.length / 2 * this.width) - ((conf.vals.length / 2) * this.padd);

        conf.vals.forEach( (val, n) => {
            const newConf = {
                x:this.x + (n * this.padd) + ((n+1) * this.width),
                y:this.y,
                hook:conf.hook,
                val:val,
                padd:this.padd,
                width:this.width
            }
            this.vizData.push(new Col(newConf));
        });
        this.stage.add(this.hook);
        //console.log(this.vizData);
    }

    render(){
        this.vizData.forEach(data => {
            data.spawn();
        })
        return true;
    }

    swap(i, j){
        if(i < 0 || j < 0 || i > this.vizData.length || j > this.vizData.length){
            throw new Error("Can not swap vizData: index error");
        }
        
        const first = this.vizData[i];
        const second = this.vizData[j];
        const x1 = first.Rect.x();
        const x2 = second.Rect.x();
        first.move(x2, null);
        second.move(x1, null);
        this.vizData[j] = first;
        this.vizData[i] = second;
    }

    move(i, j){
        const first = this.vizData[i];
        const second = this.vizData[j];
        const x1 = first.Rect.x();
        const x2 = second.Rect.x();
        first.move(x2, null);
    }

    mod(i, val){
        this.vizData[i].mod(val);
    }

    sort(order){
        order.forEach((i, n) => {
            setTimeout(any => {
                if(!i.cmd) this.swap(i.i, i.j);
            }, n * 1000);
        });
    }

    sortByValuating(order){
        console.log(order)
        order.forEach((i, n) => {
            setTimeout(any => {
                this.mod(i.i , i.val);
            }, n * 1000);
        });
    }

}