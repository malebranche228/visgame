const random = (min, max) => {
    return Math.round(Math.random() * (max - min)) + min;
}

const randomArr = (n = 10, min = 1, max = 50) => {

    let res = [];
    for(let i = 0; i < n; i++) res.push(random(min, max));
    return res;

}

const arrAsString = arr => {
    let res = "";
    arr.forEach( (val, n) => {
        res += val.toString() + (n == arr.length-1 ? "" : " ");
    });
    return res;
}

const stringToArray = str => {
    let res = [];
    let err = false;
    str.split(" ").forEach( val => {
        const data = parseInt(val);
        if(!data || isNaN(data)){
            err = true;
            return;
        }
        res.push(data);
    });
    if(err) return null;
    return res;
}