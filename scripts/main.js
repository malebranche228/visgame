
const wrap = document.querySelector('#wrap');

let width = wrap.clientWidth;
let height = wrap.clientHeight;


const ready = (viz) => {
    const queque = bubbleSort(viz);
    viz.sort(queque);
}

const main = () => {
    const stage = new Konva.Stage({
        container: 'wrap',
        width: width,
        height: height
    });

    const layer = new Konva.Layer();
    const viz = new Vizualizer({
        hook:layer,
        stage:stage,
        vals:[10,15,25,50,30,20,55, 2, 35,67],
        padd:30,
        width:35
    });

    viz.render();
    setTimeout(any => {
        ready(viz);
    }, 2000)

}

const main_ = () => {

    const arr = getArray();
    if(!arr) return;

    const stage = new Konva.Stage({
        container: 'wrap',
        width: width,
        height: height
    });

    const layer = new Konva.Layer();
    const viz = new Vizualizer({
        hook:layer,
        stage:stage,
        vals:arr,
        padd:30,
        width:35
    });

    viz.render();
    console.log(currAlgo)
    if(currAlgo == "algo-bubble"){
        q = bubbleSort(viz);
        setTimeout( any => {
            const queque = q;
            viz.sort(queque);
        }, 2000);
        return;
    }else if(currAlgo == "algo-cocktail"){
        q = cocktail(viz.vals);
        setTimeout( any => {
            const queque = q;
            viz.sort(queque);
        }, 2000);
    }else if(currAlgo == "algo-selection"){
        q = selectionSort(viz.vals);
        setTimeout( any => {
            const queque = q;
            viz.sort(queque);
        }, 2000);
    }else if(currAlgo == 'algo-combo'){
        q = combsort(viz.vals);
        setTimeout( any => {
            const queque = q;
            viz.sort(queque);
        }, 2000);
    }
    else{
        alert("Please, select sorting algorythm");
        return;
    }

    

}

window.onload = () => {

    document.querySelector("#start").addEventListener("click", event => {
        main_();
    });

    const arr = [6,5,4,3,2,1];
    //mergeSort(arr);

}